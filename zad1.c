#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <signal.h>

int main (){
   pid_t par_proc_1 = fork();
   if (	par_proc_1 == 0){																		//if it is  child process
   	pid_t child_proc_1 = fork();
   	if (child_proc_1 != 0){
   	    printf("child_proc_1: %4d \t pid: %4d \t ppid:%d\n",child_proc_1,getpid(),getppid());	
   	    return(0);
   	    }
   	     
   	    return(0);
   }
    printf("par_proc_1: %4d \t pid: %4d \t ppid:%d\n",par_proc_1,getpid(),getppid());			//parent process 1
   
    
   pid_t par_proc_ = fork();
   if ( par_proc_2 == 0){
   	pid_t child_proc_2 = fork();
   	if (child_proc_2 != 0){
   	    printf("child_proc_2: %4d \t pid: %4d \t ppid:%d\n",child_proc_2,getpid(),getppid());
   	    return(0);
   	}    
   	 	return(0);	
   }
    printf("par_proc_2: %4d \t pid: %4d \t ppid:%d\n",par_proc_2,getpid(),getppid());
    sleep(3);
    return(0);
}
